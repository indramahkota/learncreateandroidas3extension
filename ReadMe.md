# Membuat AS3 Native Extension

### 1. Menginstal Android Studio
* __Persyaratan sistem__
    - Windows 10.
    - Minimal 4GB RAM.
    - Ruang disk yang tersedia 1.5GB .
* __Download JDK dan atur Environment Variable__
    - [Download JDK](https://www.oracle.com/technetwork/java/javase/downloads/index.html) - Link untuk mengunduh java development kit.
    - Atur Environment Variable JAVA_HOME pada Advanced System Settings windows.
* __Download dan Install Android Studio__
    - [Download Android Studio](https://developer.android.com/studio/) - Link untuk mengunduh aplikasi android studio.
    - Install android studio dengan cara Next, Next, dan Finish.
    - Setelah selesai, pilih "I do not have a previous version of Android Studio or I do not want to import my settings".
* __Download dan Install komponen Android SDK dan alat lainnya__
    - Install Android SDK.
    - Install NDK-Bundle, dll.
* __Buat new Android Studio Project untuk Testing__
    - Ini akan mengecek apakah instalannya baik-baik saja dan mendownload resorce lainnya yang belum didownload.

### 2. Mencari Project ANE Open Source di Internet
Biasanya banyak proyek-proyek ActionScript Native Extension (ANE) yang dibuka sebagai open source oleh pengembang untuk berbagi kepada orang-orang yang ingin belajar. Untuk website yang biasanya menyimpan project open source bisa ditemukan di GitHub dan GitLab.


### 3. Membuat ANE untuk Android
* __Membuat ActionScript Library__
    - Gunakan VSCode sebagai IDE untuk membuat ActionSCript 3 library atau .swc.
    - Tambahkan file asconfig.json seperti dibawah ini.
        ```json
       {
            "type": "lib",
            "config": "airmobile",
            "compilerOptions": {
                "source-path": [
                    "src"
                ],
                "include-sources": [
                    "src"
                ],
                "output": "bin/MyLibrary.swc"
            }
        }
        ```
    - Buat folder src dan koding actionscript 3 didalamnya.
    - Untuk referensi pembuatan kodenya bisa membuka alamat website ini [Adobe Tutorial](https://www.adobe.com/devnet/air/articles/building-ane-ios-android-pt2.html).
    - Gunakan opsi Release Build di Task pada terminal VSCode.
    - Ambil file .swc yang berada didalam folder bin, copy file tersebut dan paste di folder yang berbeda, lalu ganti ekstensi file tersebut menjadi .rar, setelah itu ekstrak file .rar tersebut. Terdapat file library.swf didalamnya dan itu yang akan digunakan nantinya untuk membuat actionscript native extension.
* __Membuat Java Library__
    - Buka aplikasi Android Studio.
    - Pilih ``Start a new Android Studio project``.
    - Lalu akan muncul halaman baru. Ketik di ``Application Name`` dan ``Company Domain``. Pilih dan Masukkan lokasi penyimpanan proyek di ``Project location``. Ini akan menentukan id proyek yang akan dibuat contohnya ``com.indramahkota.toast`` sebagai id. Lalu klik ``Next``.
    - Pilih SDK minimal di ``Minimum SDK``, contohnya API 26. Klik ``Next``.
    - Setelah itu muncul Modal pilihan proyek. Pilih ``Add No Activity``. Klik ``Finish``.
    - Proyek Android Studio telah dimulai. Lalu tujuan untuk membuat library Java akan dimulai setelah ini.
    - Klik tombol ``File`` di pojok kiri atas, lalu ``New`` dan ``New module``.
    - Pilih ``Android library``. Klik ``Next``.
    - Lalu selanjutnya akan muncul halaman lagi baru seperti ketika mengatur id proyek. Pada ``Application/Library name``, ketik nama library contohnya ``Toast``.
    - Pada ``Module name``, ketik nama modul contohnya ``toast``. Ini akan menentukan id library yang akan dibuat contohnya ``com.indramahkota.toast``.
    - Pilih ``Minimum SDK`` dan pilih ``Next``.
    - Pada menu ``project`` di android studio, terdapat pilihan tampilan folder-folder isi project yang dibuat. Jika pilihan yang dipilih adalah ``Android`` maka folder-folder tersebut akan tersusun rapi seolah tidak banyak folder yang ada untuk memudahkan pengembang fokus hanya pada kode saja, tetapi jika pilihan yang dipilih adalah ``Package`` maka folder-folder tersebut akan terlihat banyak dan tidak tersusun rapi, ini nantinya akan ditemukan folder ``build/outputs/`` yang didalamnya terdapat ``library.aar`` yang nantinya akan kita gunakan. 
    - Proyek library telah dimulai.
    - Kita dapat mulai mengkoding .java di folder ``com.indramahkota.toast``.
    - Setelah selesai dan tidak ada error pada kode yang kita buat, pilih pada pojok kanan tengah terdapat tombol ``Gradle``.
    - Klik tombol tersebut dan akan muncul opsi-opsi didalam gradle, pilih build dan klik duakali di assembleRelease untuk membuat file library.aar.
    - Setelah selesai compile, file library.aar akan kita temukan di folder ``build/outputs/`` jika kita pilih tampilan ``Package`` pada tombol ``Project``.
    - Ambil file library.aar yang berada didalam folder tersebut, copy file tersebut dan paste di folder yang berbeda, lalu ganti ekstensi file tersebut menjadi .rar, setelah itu ekstrak file .rar tersebut. Terdapat file classes.jar didalamnya dan itu yang akan digunakan nantinya untuk membuat actionscript native extension.

### 4. Mengkompile File-file Yang Telah Dibuat Dalam Bentuk .ane
* __Mengatur Environment FLEX_HOME__
Atur Environment ``FLEX_HOME`` dengan lokasi pada AIR SDK terbaru yang dimiliki lalu berikan path  ``%FLEX_HOME%\bin``. Tes apakah environment variable sudah dibuat dengan benar caranya dengan mengetikkan mxmlc atau adt pada cmd yang dibuka dengan izin administrator.
* __Mengatur Folder Sebelum Menjalankan Fungsi Adt__
    ```
    build directory
        android
            -androidLib.jar
            -library.swf (main ActionScript library)
        -extension.xml
        -SWC for main ActionScript library
    ```
    atau secara lengkapnya
    ```
    build directory
        android
            -androidLib.jar
            -library.swf (main ActionScript library)
        ios
            -iOSLib.a
            -library.swf (main ActionScript library)
        default
            -library.swf (default library)
        -extension.xml
        -platformoptions.xml (if required)
        -SWC for main ActionScript library
    ```
* __Mengetikkan Perintah Adt__
Perintah yang digunakan untuk membuat file .ane yaitu ``adt -package -target ane Toast.ane extension.xml -swc toastlibrary.swc -platform Android-ARM -C android .`` atau secara lengkapnya ``adt -package -target ane Output.ane extension.xml -swc VolumeLib.swc -platform iPhone-ARM -C ios . -platformoptions platformoptions.xml -platform Android-ARM -C android . -platform default -C default .``
* __Menambahkan Sertifikat__
Untuk menambahkan sertifikat pada file .ane ini sangat disarankan, tetapi jika tidak juga tak apa dilakukan tanpa sertifikat. Perintah yang digunakan adalah: ``-storetype pkcs12 -keystore cert.p12 -storepass XXXX`` ditambahkan dengan perintah untuk membuat .ane diatas. Untuk membuat sertifikat, dapat dilakukan dengan adt lagi caranya dapat dilihat di file .bat FlashDevelop.
* __Kode Yang Benar Jika Menggunakan Sertifikat__
``adt -package -storetype pkcs12 -keystore certificate.p12 -target ane Toast.ane extension.xml -swc toastlibrary.swc -platform Android-ARM -C android .``